package plugins.danyfel80.numbers.generator;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Random generator plugins. When installed, this plugin allows access to the
 * following blocks:
 * <ul>
 * <li>GenerateRandomInteger: Generator for random integer values.</li>
 * <li>GenerateRandomDouble: Generator for random double values.</li>
 * </ul>
 * 
 * @author Daniel Felipe Gonzalez Obando
 */
public class RandomGenerators extends Plugin implements PluginLibrary {}
