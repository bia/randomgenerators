/*
 * Copyright 2010-2018 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.danyfel80.numbers.generator;

import java.util.Random;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarDouble;

/**
 * This block plugin allows users to obtain a random real value within given
 * minimum (inclusive) and maximum (exclusive) values.
 *
 * @author Daniel Felipe Gonzalez Obando
 */
public class GenerateRandomDouble extends Plugin implements PluginLibrary, Block {

	private static Random RANDOM_GENERATOR = new Random();

	private VarDouble inVarMin;
	private VarDouble inVarMax;

	private VarDouble outVarValue;

	@Override
	public void declareInput(VarList inputMap) {
		inVarMin = new VarDouble("Minimum value", 0d);
		inVarMax = new VarDouble("Maximum value", 1d);
		inputMap.add(inVarMin.getName(), inVarMin);
		inputMap.add(inVarMax.getName(), inVarMax);
	}

	@Override
	public void declareOutput(VarList outputMap) {
		outVarValue = new VarDouble("Value", 0d);
		outputMap.add(outVarValue.getName(), outVarValue);
	}

	@Override
	public void run() {
		Double min = inVarMin.getValue(true);
		Double max = inVarMax.getValue(true);
		Double value = min + ((max - min) * RANDOM_GENERATOR.nextDouble());
		outVarValue.setValue(value);
	}

}
