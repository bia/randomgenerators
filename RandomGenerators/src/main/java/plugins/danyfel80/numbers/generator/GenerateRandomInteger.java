/*
 * Copyright 2010-2018 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.danyfel80.numbers.generator;

import java.util.Random;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarInteger;

/**
 * This block plugin allows users to obtain a random integer value within given
 * minimum (inclusive) and maximum (exclusive) values.
 *
 * @author Daniel Felipe Gonzalez Obando
 */
public class GenerateRandomInteger extends Plugin implements PluginLibrary, Block {

	private static Random RANDOM_GENERATOR = new Random();

	private VarInteger inVarMin;
	private VarInteger inVarMax;

	private VarInteger outVarValue;

	@Override
	public void declareInput(VarList inputMap) {
		inVarMin = new VarInteger("Minimum value", 0);
		inVarMax = new VarInteger("Maximum value", 100);
		inputMap.add(inVarMin.getName(), inVarMin);
		inputMap.add(inVarMax.getName(), inVarMax);
	}

	@Override
	public void declareOutput(VarList outputMap) {
		outVarValue = new VarInteger("Value", 0);
		outputMap.add(outVarValue.getName(), outVarValue);
	}

	@Override
	public void run() {
		Integer min = inVarMin.getValue(true);
		Integer max = inVarMax.getValue(true);
		Integer value = min + (RANDOM_GENERATOR.nextInt(max - min));
		outVarValue.setValue(value);
	}

}
