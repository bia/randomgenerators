/*
 * Copyright 2010-2018 Institut Pasteur.
 * 
 * This file is part of Icy.
 * 
 * Icy is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Icy is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Icy. If not, see <http://www.gnu.org/licenses/>.
 */
package plugins.danyfel80.image.generator;

import java.awt.Dimension;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.sequence.SequenceUtil;
import icy.type.point.Point5D;
import icy.type.rectangle.Rectangle5D;
import icy.type.rectangle.Rectangle5D.Integer;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.vars.lang.VarSequence;
import vars.geom.VarDimension;

/**
 * @author Daniel Felipe Gonzalez Obando
 */
public class RandomSubimageGenerator extends Plugin implements PluginLibrary, Block {

	VarSequence varInputSequence;
	VarDimension varPaddingDimension;
	VarSequence varOutputSequence;

	@Override
	public void declareInput(VarList inputMap) {
		varInputSequence = new VarSequence("Sequence", null);
		varPaddingDimension = new VarDimension("Padding dimension", new Dimension(0, 0));
		inputMap.add(varInputSequence.getName(), varInputSequence);
		inputMap.add(varPaddingDimension.getName(), varPaddingDimension);
	}

	@Override
	public void declareOutput(VarList outputMap) {
		varOutputSequence = new VarSequence("sub image", null);
		outputMap.add(varOutputSequence.getName(), varOutputSequence);
	}

	@Override
	public void run() {
		Sequence inputSequence = varInputSequence.getValue(true);
		Dimension padding = varPaddingDimension.getValue(true);

		Rectangle5D.Integer region = getRandomRegion(inputSequence, padding);
		Sequence outputSequence = generateSubSequence(inputSequence, region);
		varOutputSequence.setValue(outputSequence);
	}

	private Integer getRandomRegion(Sequence inputSequence, Dimension padding) {
		int x = (int) ((padding.width * 2) * Math.random());
		int y = (int) ((padding.height * 2) * Math.random());
		int sizeX = inputSequence.getSizeX() - (padding.width * 2);
		int sizeY = inputSequence.getSizeY() - (padding.width * 2);

		return new Rectangle5D.Integer(x, y, 0, 0, 0, sizeX, sizeY, inputSequence.getSizeZ(), inputSequence.getSizeT(),
				inputSequence.getSizeC());
	}

	private Sequence generateSubSequence(Sequence inputSequence, Integer region) {
		Sequence subSequence = SequenceUtil.getSubSequence(inputSequence, region);
		for (ROI roi: inputSequence.getROIs()) {
			if (region.intersects(roi.getBounds5D())) {
				ROI roiCopy = roi.getCopy();
				Point5D copyPosition = roi.getPosition5D();
				copyPosition.setX(copyPosition.getX() - region.getX());
				copyPosition.setY(copyPosition.getY() - region.getY());
				copyPosition.setZ(copyPosition.getZ() - region.getZ());
				copyPosition.setT(copyPosition.getT() - region.getT());
				copyPosition.setC(copyPosition.getC() - region.getC());
				roiCopy.setPosition5D(copyPosition);
				subSequence.addROI(roiCopy);
			}
		}
		return subSequence;
	}

}
